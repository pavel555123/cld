//Swiper
const swiper = new Swiper('.swiper', {
    pagination: {
        el: '.swiper-pagination',
    },

});

//Work
document.addEventListener('DOMContentLoaded', function () {
    document.querySelectorAll('.work-list__step').forEach(function (e) {
        e.addEventListener('click', function (e) {
            const tab = e.currentTarget.dataset.path;
            document.querySelectorAll('.tab-content').forEach(function (e) {
                e.classList.remove('tab-content--active')
                document.querySelector(`[data-target='${tab}']`).classList.add('tab-content--active');
            });
        });

        e.addEventListener('click', function (e) {
            const tabDefault = e.currentTarget.dataset.default;
            document.querySelectorAll('.work-list__step').forEach(function (e) {
                e.classList.remove('work-list__step--default')
                document.querySelector(`[data-path='${tabDefault}']`).classList.add('work-list__step--default');
            });
        });
    });
});

//Accordion
$(function () {
    $("#accordion").accordion({
        active: false,
        collapsible: true,
        heightStyle: 'content',
        icons: false,
        create: function (event, ui) {
            $("#accordion").attr('tabindex', 0);
        }
    });
});

//Burger
const burgerBtn = document.querySelector('.burger');
const menuClosed = document.querySelector('.menu-close');
const menuBurger = document.querySelector('.header__nav');

burgerBtn.addEventListener('click', () => {
    menuBurger.classList.add('header__burger-active');
});

menuClosed.addEventListener('click', () => {
    menuBurger.classList.remove('header__burger-active');
});